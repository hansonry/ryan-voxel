#include <stdlib.h>
#include "VecArray.h"

void VecArray_Init(struct vecArray * array, 
                   size_t width, size_t height, size_t depth,
                   float scale,
                   const struct vec3f * offset)
{
   struct vec3f defaultOffset = {0, 0, 0};
   if(offset == NULL)
   {
      offset = &defaultOffset;
   }
   array->width  = width;
   array->height = height;
   array->depth  = depth;
   array->xDelta = 1;
   array->yDelta = array->width;
   array->zDelta = array->yDelta * array->height;
   array->size   = array->zDelta * array->depth;
   array->array  = malloc(sizeof(struct vec3f) * array->size);
   size_t x, y, z, index = 0;
   for(z = 0; z < array->depth; z ++)
   {
      for(y = 0; y < array->height; y ++)
      {
         for(x = 0; x < array->width; x ++)
         {
            array->array[index].x = (x * scale) + offset->x;
            array->array[index].y = (y * scale) + offset->y;
            array->array[index].z = (z * scale) + offset->z;
            index ++;
         }
      }
   }
}

void VecArray_Free(struct vecArray * array)
{
   free(array->array);
   array->array  = NULL;
   array->width  = 0;
   array->height = 0;
   array->depth  = 0;
   array->xDelta = 0;
   array->yDelta = 0;
   array->zDelta = 0;
   array->size   = 0;
   
}

static inline
size_t GetIndexBetween(const struct vecArray * array,
                       size_t aIndex, size_t bIndex)
{
   size_t diff = bIndex - aIndex;
   size_t deltas[3] = {array->zDelta, array->yDelta, array->xDelta};
   size_t deltaIndex = 3;
   size_t coordDelta = 0;
   size_t coordDiff = 0;
   size_t newCoordDiff = 0;
   // Find coordDelta
   for(deltaIndex = 0; deltaIndex < 3; deltaIndex++)
   {
      if(diff > deltas[deltaIndex])
      {
         coordDelta = deltas[deltaIndex];
         break;
      }
   }
   if(diff % coordDelta != 0)
   {
      // The two indices don't share an access
      return array->size;
   }
   coordDiff = diff / coordDelta;
   if(coordDiff < 2)
   {
      // The two indices are too close to each other to have something in between
      return array->size;
   }
   newCoordDiff = coordDiff / 2;
   return aIndex + (newCoordDiff * coordDelta);
    
}

size_t VecArray_GetIndexBetween(const struct vecArray * array,
                                size_t aIndex, size_t bIndex)
{
   if(!VecArray_IsValidIndex(array, aIndex) ||
      !VecArray_IsValidIndex(array, bIndex))
   {
      return array->size;
   }
   if(aIndex > bIndex)
   {
      return GetIndexBetween(array, bIndex, aIndex);
   }
   return GetIndexBetween(array, aIndex, bIndex);
}

