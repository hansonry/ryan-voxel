#ifndef __VEC3F_H__
#define __VEC3F_H__

struct vec3f
{
   float x;
   float y;
   float z;
};

static inline
struct vec3f * Vec3f_Copy(struct vec3f * dest, const struct vec3f * src)
{
   dest->x = src->x;
   dest->y = src->y;
   dest->z = src->z;
   return dest;
}

static inline
struct vec3f * Vec3f_Set(struct vec3f * v, float x, float y, float z)
{
   v->x = x;
   v->y = y;
   v->z = z;
   return v;
}

static inline
struct vec3f * Vec3f_Zero(struct vec3f * v)
{
   v->x = 0;
   v->y = 0;
   v->z = 0;
   return v;
}




struct vec3f * Vec3f_Add(struct vec3f * out, 
                         const struct vec3f * a, 
                         const struct vec3f * b);

struct vec3f * Vec3f_Subtract(struct vec3f * out, 
                              const struct vec3f * a, 
                              const struct vec3f * b);

float Vec3f_Length2(const struct vec3f * v);
float Vec3f_Length(const struct vec3f * v);

struct vec3f * Vec3f_Normalize(struct vec3f * out, 
                               const struct vec3f * v);


#endif // __VEC3F_H__

