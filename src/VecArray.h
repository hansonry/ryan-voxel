#ifndef __VECARRAY_H__
#define __VECARRAY_H__
#include <stddef.h>
#include <stdbool.h>
#include "Vec3f.h"

struct vecArray
{
   struct vec3f * array;
   size_t width;
   size_t height;
   size_t depth;
   size_t xDelta;
   size_t yDelta;
   size_t zDelta;
   size_t size;
};

void VecArray_Init(struct vecArray * array, 
                   size_t width, size_t height, size_t depth,
                   float scale,
                   const struct vec3f * offset);

void VecArray_Free(struct vecArray * array);

size_t VecArray_GetIndexBetween(const struct vecArray * array,
                                size_t aIndex, size_t bIndex);

static inline
bool VecArray_IsValidIndex(const struct vecArray * array, size_t index)
{
   return index < array->size;
}

static inline
size_t VecArray_GetIndex(const struct vecArray * array, 
                         size_t x, size_t y, size_t z)
{
   if(x >= array->width || y >= array->height || z >= array->depth)
   {
      return array->size;
   }
   return x + (y * array->yDelta) + (z * array->zDelta);
}

static inline
struct vec3f * VecArray_Get(struct vecArray * array,
                            size_t x, size_t y, size_t z)
{
   size_t index = VecArray_GetIndex(array, x, y, z);
   if(!VecArray_IsValidIndex(array, index))
   {
      return NULL;
   }
   return &array->array[index];
}



#endif //__VECARRAY_H__
