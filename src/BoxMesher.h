#ifndef __BOXMESHER_H__
#define __BOXMESHER_H__
#include "InternalVoxelFormat.h"
#include "Mesh.h"

void BoxMesher_Mesh(struct mesh * mesh, const struct config * config);


#endif // __BOXMESHER_H__

