#include <stdio.h>
#include "Mesher.h"
#include "BoxMesher.h"
#include "NetMesher.h"

void Mesher_Mesh(struct mesh * mesh, const struct config * config)
{
   switch(config->meshType)
   {
   case eMT_Box:
      BoxMesher_Mesh(mesh, config);
      break;
   case eMT_Net:
      NetMesher_Mesh(mesh, config);
      break;
   default:
      fprintf(stderr, "Error: Unsupported mesh type: %d\n", config->meshType);
      break;
   }
}