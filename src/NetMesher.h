#ifndef __NETMESHER_H__
#define __NETMESHER_H__
#include "InternalVoxelFormat.h"
#include "Mesh.h"

void NetMesher_Mesh(struct mesh * mesh, const struct config * config);

#endif // __NETMESHER_H__
