#ifndef __MESH_H__
#define __MESH_H__

#include "Vec3f.h"
#include "stddef.h"

struct mesh;


struct mesh * Mesh_Create(void);
void Mesh_Destroy(struct mesh * mesh);

void Mesh_AddFace(struct mesh * mesh, const struct vec3f ** vects, size_t count);

void Mesh_MakeObjFile(struct mesh * mesh, const char * name);

void Mesh_Solidify(struct mesh * mesh);

#endif // __MESH_H__

