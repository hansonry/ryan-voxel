#ifndef __INTERNALVOXELFORMAT_H__
#define __INTERNALVOXELFORMAT_H__

#include <stdbool.h>
#include <stddef.h>

enum meshType
{
   eMT_Box,
   eMT_Net
};

struct color
{
   float r;
   float g;
   float b;
};

struct brush
{
   bool isEmpty;
   struct color color;
};

struct boxSetting
{
   int ignore;
};
struct netSetting
{
   size_t smallestOctCube;
   size_t maxSmoothingSteps;
};

struct config
{
   const char * name;
   size_t width;
   size_t height;
   size_t depth;
   const struct brush ** data;

   enum meshType meshType;
   union meshSettings
   {
      struct boxSetting box;
      struct netSetting net;
   } settings;
};


#endif // __INTERNALVOXELFORMAT_H__

