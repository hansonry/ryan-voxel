#include <stdlib.h>
#include "Mesher.h"
#include "List.h"
#include "VecArray.h"

enum cubeSide
{
   eCS_Bottom,
   eCS_Top,
   eCS_Left,
   eCS_Right,
   eCS_Back,
   eCS_Front,
   eCS_Count
};

struct cubeFace
{
   size_t p[3];
   size_t d[2];

};

LIST_MAKE(cf, struct cubeFace)

struct meshData
{
   const struct config * config;
   struct vecArray meshVerts;
   struct list_cf faceMaps[eCS_Count];
   size_t wh;
   size_t size;
};

static const char CubeOffsets[6][15] = {
   /* eCS_Bottom */ { 
      0, -1, 0, // relative to 
      0, 0, 0,
      1, 0, 0,
      1, 0, 1,
      0, 0, 1,
   },
   /* eCS_Top */ {
      0, 1, 0, // relative to
      0, 1, 0,
      0, 1, 1,
      1, 1, 1,
      1, 1, 0,
   },
   /* eCS_Left */ {
      -1, 0, 0, // relative to
      0, 0, 0,
      0, 0, 1,
      0, 1, 1,
      0, 1, 0,
   },
   /* eCS_Right */ {
      1, 0, 0, // relative to
      1, 0, 0,
      1, 1, 0,
      1, 1, 1,
      1, 0, 1,
   },
   /* eCS_Back */ {
      0, 0, 1, // relative to
      0, 0, 1,
      1, 0, 1,
      1, 1, 1,
      0, 1, 1,
   },
   /* eCS_Front */ {
      0, 0, -1, // relative to
      0, 0, 0,
      0, 1, 0,
      1, 1, 0,
      1, 0, 0,
   }
};

static const unsigned char FaceDirTable[6][3] = {
   /* eCS_Bottom */ { 
      0, 2, 1,
   },
   /* eCS_Top */ {
      0, 2, 1,
   },
   /* eCS_Left */ {
      1, 2, 0, 
   },
   /* eCS_Right */ {
      1, 2, 0,
   },
   /* eCS_Back */ {
      0, 1, 2,
   },
   /* eCS_Front */ {
      0, 1, 2,
   }
};

static inline
void createFaceMaps(struct meshData * meshData)
{
   enum cubeSide cs;
   for(cs = 0; cs < eCS_Count; cs++)
   {
      List_Init_cf(&meshData->faceMaps[cs],0 , 0);
   }
}

static inline
void freeFaceMaps(struct meshData * meshData)
{
   enum cubeSide cs;
   for(cs = 0; cs < eCS_Count; cs++)
   {
      List_Free_cf(&meshData->faceMaps[cs]);
   }
}

static inline
void createVectorArray(struct meshData * meshData)
{
   const struct config * config = meshData->config;
   VecArray_Init(&meshData->meshVerts, 
                 config->width  + 1,
                 config->height + 1,
                 config->depth  + 1,
                 1, NULL);
}

static inline
size_t getIndexOfVectorArray(const struct meshData * meshData, 
                             size_t x, size_t y, size_t z)
{
   return VecArray_GetIndex(&meshData->meshVerts, x, y, z);
}

static inline 
const struct vec3f * getVertexOfVectorArray(const struct meshData * meshData, 
                                            const char * offsets, 
                                            size_t x, size_t y, size_t z)
{
   const struct vec3f * array = meshData->meshVerts.array;
   return &array[getIndexOfVectorArray(meshData, x + offsets[0], 
                                                 y + offsets[1], 
                                                 z + offsets[2])];
}

static inline
const struct vec3f * getVertexOfCubeFace(const struct meshData * meshData,
                                         const enum cubeSide cs,
                                         const struct cubeFace * face,
                                         unsigned char quadIndex)
{
   const struct vec3f * array = meshData->meshVerts.array;
   const char * offsets = &CubeOffsets[cs][3 + (quadIndex * 3)];
   const unsigned char *  fdt = FaceDirTable[cs];
   unsigned char d[3];
   d[fdt[0]] = face->d[0];
   d[fdt[1]] = face->d[1];
   d[fdt[2]] = 1;
   size_t x = face->p[0] + offsets[0] * d[0];
   size_t y = face->p[1] + offsets[1] * d[1];
   size_t z = face->p[2] + offsets[2] * d[2];
   return &array[getIndexOfVectorArray(meshData, x, y, z)];
   
}

static inline
size_t getCubeIndex(const struct meshData * meshData, 
                    size_t x, size_t y, size_t z)
{
   const struct config * config = meshData->config;
   if(x >= config->width || y >= config->height || z >= config->depth)
   {
      return meshData->size;
   }
   return x + y * config->width + z * meshData->wh;
}

static inline
void makeFaceMasks(struct meshData * meshData)
{
   const struct config * config = meshData->config;
   size_t x, y, z, index = 0;
   for(z = 0; z < config->depth; z++)
   {
      for(y = 0; y < config->height; y++)
      {
         for(x = 0; x < config->width; x++)
         {
            const struct brush * brush = config->data[index];
            if(!brush->isEmpty)
            {
               enum cubeSide cs;
               for(cs = 0; cs < eCS_Count; cs ++)
               {
                  const char * os = CubeOffsets[cs];
                  size_t nextToIndex = getCubeIndex(meshData, x + os[0], 
                                                              y + os[1], 
                                                              z + os[2]);
                  if(nextToIndex >= meshData->size || 
                     config->data[nextToIndex]->isEmpty)
                  {
                     struct cubeFace *cf = List_AddEmpty_cf(&meshData->faceMaps[cs], NULL);
                     cf->p[0] = x;
                     cf->p[1] = y;
                     cf->p[2] = z;
                     cf->d[0] = 1;
                     cf->d[1] = 1;
                     
                  }
               }
            }
            index ++;
         }
      }
   }
}



static inline
bool mergeFacesIfPossible(struct cubeFace * into, 
                          const struct cubeFace * from, 
                          enum cubeSide side,
                          unsigned char dir)
{
   const unsigned char * f = FaceDirTable[side];
   const unsigned char m = f[2];
   const unsigned char n = f[dir];
   const unsigned char notDir = ((dir == 0) ? 1 : 0);
   const unsigned char o = f[notDir];
      
   if(into->p[m] == from->p[m] &&
      into->d[notDir] == from->d[notDir] &&
      into->p[o] == from->p[o])
   {
      if(into->p[n] + into->d[dir] == from->p[n])
      {
         into->d[dir] += from->d[dir];
         return true;
         
      }
      if(from->p[n] + from->d[dir] == into->p[n])
      {
         into->d[dir] += from->d[dir];
         into->p[0] = from->p[0];
         into->p[1] = from->p[1];
         into->p[2] = from->p[2];
         return true;
      }
   }
   return false;
}

static inline
void optimizeFacesInDirection(struct meshData * meshData, enum cubeSide cs,
                              unsigned char dir)
{
   struct list_cf * list = &meshData->faceMaps[cs];
   size_t i;
   for(i = 0; i < List_GetCount_cf(list); i++)
   {
      bool merge = true;
      while(merge)
      {
         merge = false;
         size_t k, count;
         struct cubeFace * faces = List_Get_cf(list, &count);
         for(k = count - 1; k >= i + 1; k--)
         {
            if(mergeFacesIfPossible(&faces[i], &faces[k], cs, dir))
            {
               List_RemoveFast_cf(list, k);
               merge = true;
            }
         }
      }
   }
}

static inline
void optimizeFaces(struct meshData * meshData)
{
   enum cubeSide cs;
   for(cs = 0; cs < eCS_Count; cs++)
   {
      optimizeFacesInDirection(meshData, cs, 0);
      optimizeFacesInDirection(meshData, cs, 1);
   }
}

static inline
void makeFacesFromFaceMask(struct meshData * meshData, struct mesh * mesh)
{
   enum cubeSide cs;
   for(cs = 0; cs < eCS_Count; cs++)
   {
      struct list_cf * faceMap = &meshData->faceMaps[cs];
      size_t i, count;
      const struct cubeFace * faces = List_Get_cf(faceMap, &count);
      
      for(i = 0; i < count; i++)
      {
         const struct cubeFace * cf = &faces[i];
         const struct vec3f * face[] = {
            getVertexOfCubeFace(meshData, cs, cf, 0),
            getVertexOfCubeFace(meshData, cs, cf, 1),
            getVertexOfCubeFace(meshData, cs, cf, 2),
            getVertexOfCubeFace(meshData, cs, cf, 3),
         };
         Mesh_AddFace(mesh, face, 4);
      }
   }
}


void BoxMesher_Mesh(struct mesh * mesh, const struct config * config)
{
   size_t wh = config->width * config->height;
   struct meshData meshData = {
      .config = config,
      .wh = wh,
      .size = wh * config->depth
   };
   createVectorArray(&meshData);
   createFaceMaps(&meshData);
   makeFaceMasks(&meshData);
   optimizeFaces(&meshData);
   makeFacesFromFaceMask(&meshData, mesh);
   Mesh_Solidify(mesh);
   VecArray_Free(&meshData.meshVerts);
   createFaceMaps(&meshData);
}

