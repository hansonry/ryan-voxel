#include <stdlib.h>
#include <stdio.h>
#include <stdbool.h>
#include <string.h>
#include <cjson/cJSON.h>
#include "JsonLoader.h"
#include "List.h"

#define NAME_BRUSHES "brushes"
#define NAME_ISEMPTY "isEmpty"
#define NAME_TYPE    "type"
#define NAME_WIDTH   "width"
#define NAME_HEIGHT  "height"
#define NAME_DEPTH   "depth"
#define NAME_DATA    "data"
#define NAME_NAME    "name"

struct brushData
{
   char name;
   struct brush brush;
};

LIST_MAKE(brush, struct brushData)


struct jsonLoader
{
   struct list_brush brushList;
   struct config config;
   const struct brush ** data; 
   char * name;
};

static inline
char * LoadTextFileIntoString(const char * filename)
{
   FILE * fh = fopen(filename, "rb");
   size_t length = 0;
   char * buffer = NULL;
   if(fh == NULL)
   {
      return NULL;
   }
   // TODO: maybe harden this against errors
   fseek(fh, 0, SEEK_END);
   length = ftell(fh);
   fseek(fh, 0, SEEK_SET);
   buffer = malloc(length + 1);
   fread(buffer, 1, length, fh);
   buffer[length] = '\0';
   fclose(fh);
   return buffer;
}



struct jsonLoader * JsonLoader_Load(const char * filename)
{
   char * text = LoadTextFileIntoString(filename);
   if(text == NULL)
   {
      return NULL;
   }
   struct jsonLoader * loader = JsonLoader_LoadString(text);
   free(text);
   return loader;
}

static inline
bool ParseBrushes(struct jsonLoader * loader, const cJSON * jBrushes)
{
   const cJSON * child = jBrushes->child;
   while(child != NULL)
   {
      if(strlen(child->string) > 1)
      {
         fprintf(stderr, "Bad Brush name: \"%s\". Expected brush names to be only one character long\n", child->string);
         return false;
      }
      struct brushData * brush = List_AddEmpty_brush(&loader->brushList, NULL);
      brush->name = child->string[0];
      
      const cJSON * jIsEmpty = cJSON_GetObjectItemCaseSensitive(child, NAME_ISEMPTY); 
      if(cJSON_IsBool(jIsEmpty))
      {
         brush->brush.isEmpty = cJSON_IsTrue(jIsEmpty);
      }
      else
      {
         fprintf(stderr, "Expected \"" NAME_ISEMPTY "\" to be a boolean\n");
         return false;
      }
      child = child->next;
   }
   return true;
}

static inline 
bool ReadNumber(double * number, const cJSON * json, const char * name, bool required, double defaultValue)
{
   const cJSON * jVal = cJSON_GetObjectItemCaseSensitive(json, name);
   if(jVal == NULL)
   {
      if(required)
      {
         fprintf(stderr, "\"%s\" missing. Expected \"%s\" to be a number\n", name, name);
         return false;
      }
      else
      {
         (*number) = defaultValue;
         return true;
      }
   } 
   
   if(!cJSON_IsNumber(jVal))
   {
      fprintf(stderr, "Expected \"%s\" to be a number\n", name);
      return false;
   }
   (*number) = jVal->valuedouble;
   return true;
}

static inline 
bool ReadString(const char ** str, const cJSON * json, const char * name, bool required, const char *  defaultValue)
{
   const cJSON * jVal = cJSON_GetObjectItemCaseSensitive(json, name);
   if(jVal == NULL)
   {
      if(required)
      {
         fprintf(stderr, "\"%s\" missing. Expected \"%s\" to be a string\n", name, name);
         return false;
      }
      else
      {
         (*str) = defaultValue;
         return true;
      }
   } 
   
   if(!cJSON_IsString(jVal))
   {
      fprintf(stderr, "Expected \"%s\" to be a string\n", name);
      return false;
   }
   (*str) = jVal->valuestring;
   return true;
}

static inline
bool ParseDimention(size_t * outValue, const cJSON * root, const char * name)
{
   double temp;
   if(ReadNumber(&temp, root, name, true, 0))
   {
      (*outValue) = (size_t)temp;
      return true;
   }
   return false;
}
static inline
const struct brush * FindBrushByName(struct jsonLoader * loader, char name)
{
   size_t i, count;
   struct brushData * brushes = List_Get_brush(&loader->brushList, &count);
   for(i = 0; i < count; i++)
   {
      if(brushes[i].name == name)
      {
         return &brushes[i].brush;
      }
   }
   return NULL;
}

static inline
bool ParseData(struct jsonLoader * loader, const cJSON * jData) 
{
   struct config * config = &loader->config;
   size_t size = config->width * config->height * config->depth;
   size_t index = 0;
   const cJSON * jStr = NULL;

   loader->data = malloc(sizeof(struct brush*) * size);
   config->data = loader->data;

   cJSON_ArrayForEach(jStr, jData)
   {
      const char * c = jStr->valuestring;
      while(*c != '\0')
      {
         if(index >= size)
         {
            fprintf(stderr, "Too much data for size: %s\n", c);
            return 0;
         }

         const struct brush * brush = FindBrushByName(loader, *c);
         if(brush == NULL)
         {
            fprintf(stderr, "Didn't find brush named \"%c\"\n", *c);
            return false;
         }
         loader->data[index] = brush;
         c++;
         index ++;
      }
   }

   if(index < size)
   {
      fprintf(stderr, "To little data for size %d of expected %d\n", (int)index, (int)size);
      return false;
   }
   

   return true;
  
}


static inline
bool ParseBoxSettings(struct boxSetting * sBox, const cJSON * root)
{
   (void)sBox;
   (void)root;
   // Set Defaults
   // Read from json
   return true;
}

static inline
bool ParseNetSettings(struct netSetting * sNet, const cJSON * root)
{
   double temp = 0;
   // Set Defautls


   // Read from json

   if(ReadNumber(&temp, root, "smallestOctCube", false, 1))
   {
      sNet->smallestOctCube = (size_t)temp;
   }
   else
   {
      return false;
   }
   
   if(ReadNumber(&temp, root, "maxSmoothingSteps", false, 1))
   {
      sNet->maxSmoothingSteps = (size_t)temp;
   }
   else
   {
      return false;
   }

   return true;
}

static inline
bool Parse(struct jsonLoader * loader, const cJSON * root)
{
   struct config * config = &loader->config;
   const char * temp = NULL;

   if(ReadString(&temp, root, NAME_NAME, false, "unnamed"))
   {
      loader->name = strdup(temp);
      config->name = loader->name;
   }
   else
   {
      return false;
   }
   const cJSON * jBrushes = cJSON_GetObjectItemCaseSensitive(root, NAME_BRUSHES);
   if(!cJSON_IsObject(jBrushes))
   {
      fprintf(stderr, "Expected \"" NAME_BRUSHES "\" to be an object\n");
      return false;
   }
   if(!ParseBrushes(loader, jBrushes))
   {
      return false;
   }

   const cJSON * jType = cJSON_GetObjectItemCaseSensitive(root, NAME_TYPE);
   if(cJSON_IsString(jType))
   {
      const char * sType = jType->valuestring;
      if(strcmp(sType, "box") == 0)
      {
         config->meshType = eMT_Box; 
         if(!ParseBoxSettings(&config->settings.box, root))
         {
            return false;
         }
      }
      else if(strcmp(sType, "net") == 0)
      {
         config->meshType = eMT_Net;
         if(!ParseNetSettings(&config->settings.net, root))
         {
            return false;
         }
      }
      else
      {
         fprintf(stderr, "Expected \"" NAME_TYPE "\" to be either \"box\" or \"net\" instead of \"%s\"\n", sType);
         return false;
      }
   }
   else
   {
      fprintf(stderr, "Expected \"" NAME_TYPE "\" to be a string\n");
      return false;
   }

   if(!ParseDimention(&config->width, root, NAME_WIDTH))
   {
      return false;
   }
   if(!ParseDimention(&config->height, root, NAME_HEIGHT))
   {
      return false;
   }
   if(!ParseDimention(&config->depth, root, NAME_DEPTH))
   {
      return false;
   }

   const cJSON * jData = cJSON_GetObjectItemCaseSensitive(root, NAME_DATA);
   if(!cJSON_IsArray(jData))
   {
      fprintf(stderr, "Expected \"" NAME_DATA "\" to be an array of strings\n");
      return false;
   }

   if(!ParseData(loader, jData))
   {
      return false;
   }

   return true;
}

struct jsonLoader * JsonLoader_LoadString(const char * string)
{
   struct jsonLoader * loader = NULL;
   cJSON *json = cJSON_Parse(string);
   if(json == NULL)
   {
      const char * errPtr = cJSON_GetErrorPtr();
      fprintf(stderr, "Error Parsing JSON\n");
      if(errPtr != NULL)
      {
         fprintf(stderr, "Error before: %s\n", errPtr);
      }
   }
   else
   {
      loader = malloc(sizeof(struct jsonLoader));
      List_Init_brush(&loader->brushList, 0, 0);
      loader->data = NULL;
      loader->name = NULL;
      if(!Parse(loader, json))
      {
         JsonLoader_Destroy(loader);
         loader = NULL;
      }
      cJSON_Delete(json);
   }
   return loader;
}

void JsonLoader_Destroy(struct jsonLoader * loader)
{
   if(loader->data != NULL)
   {
      free(loader->data);
   }
   if(loader->name != NULL)
   {
      free(loader->name);
   }
   List_Free_brush(&loader->brushList);
   free(loader);
}


struct config * JsonLoader_GetConfig(struct jsonLoader * loader)
{
   return &loader->config;
}

