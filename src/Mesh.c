#include <stdio.h>
#include <string.h>
#include <stdlib.h>


#include "Mesh.h"

#include "List.h"

struct vec3fPair
{
   struct vec3f v;
   const struct vec3f * vp;
};

LIST_MAKE(v3fp, struct vec3fPair)

struct triangle
{
   size_t vs[3];
};

LIST_MAKE(tri, struct triangle)

struct mesh
{
   struct list_v3fp vs;
   struct list_tri  tris;
};



struct mesh * Mesh_Create(void)
{
   struct mesh * mesh = malloc(sizeof(struct mesh));
   List_Init_v3fp(&mesh->vs, 0, 0);
   List_Init_tri(&mesh->tris, 0, 0);
   return mesh;
}

void Mesh_Destroy(struct mesh * mesh)
{
   List_Free_v3fp(&mesh->vs);
   List_Free_tri(&mesh->tris);
   free(mesh);
}

static inline
size_t Mesh_FindVec3f(struct mesh * mesh, const struct vec3f * v)
{
   size_t i, count;
   struct vec3fPair * vsp = List_Get_v3fp(&mesh->vs, &count);
   for(i = 0; i < count; i++)
   {
      if(v == vsp[i].vp)
      {
         return i;
      }
   }
   return count;
}


static inline
size_t Mesh_AddVec3f(struct mesh * mesh, const struct vec3f * v)
{
   size_t index = Mesh_FindVec3f(mesh, v);
   if(!List_IsValidIndex_v3fp(&mesh->vs, index))
   {
      struct vec3fPair * p = List_AddEmpty_v3fp(&mesh->vs, &index);
      p->vp = v;
      (void)Vec3f_Zero(&p->v);
   }
   return index;
}

void Mesh_AddFace(struct mesh * mesh, const struct vec3f ** vects, size_t count)
{
   size_t i;
   size_t root = Mesh_AddVec3f(mesh, vects[0]);
   size_t prev = Mesh_AddVec3f(mesh, vects[1]);
   for(i = 2; i < count; i ++)
   {
      struct triangle tri;
      size_t current = Mesh_AddVec3f(mesh, vects[i]);
      tri.vs[0] = root;
      tri.vs[1] = prev;
      tri.vs[2] = current;
      List_AddCopy_tri(&mesh->tris, &tri, NULL);

      prev = current;
      
   }
}

static inline
char * stringAppend(const char * a, const char * b)
{
   size_t aLen = strlen(a);
   size_t fullLen = aLen + strlen(b);
   char * newStr = malloc(fullLen + 1);
   strcpy(newStr, a);
   strcpy(newStr + aLen, b);
   return newStr;
}


void Mesh_MakeObjFile(struct mesh * mesh, const char * name)
{
   size_t i, count;
   const struct vec3fPair * vects;
   struct triangle * tris;
   char * filename = stringAppend(name, ".obj");

   FILE *fh = fopen(filename, "w");
   
   fprintf(fh, "o %s\n\n", name);

   vects = List_Get_v3fp(&mesh->vs, &count);
   for(i = 0; i < count; i++)
   {
      const struct vec3f * v = &vects[i].v;
      fprintf(fh, "v %f %f %f\n", v->x, v->y, v->z);
   }

   fprintf(fh, "\n");

   tris = List_Get_tri(&mesh->tris, &count);
   for(i = 0; i < count; i++)
   {
      const struct triangle * tri = &tris[i];
      fprintf(fh, "f %lu %lu %lu\n", (unsigned long)(tri->vs[0] + 1), 
                                     (unsigned long)(tri->vs[1] + 1), 
                                     (unsigned long)(tri->vs[2] + 1));
   }

   fclose(fh);

   free(filename);   
}

void Mesh_Solidify(struct mesh * mesh)
{
   size_t i, count;
   struct vec3fPair * pairs = List_Get_v3fp(&mesh->vs, &count);
   for(i = 0; i < count; i ++)
   {
      if(pairs[i].vp != NULL)
      {
         (void)Vec3f_Copy(&pairs[i].v, pairs[i].vp);
         pairs[i].vp = NULL;
      }
   }
}

