#ifndef __CONFIGWRAPPER_H__
#define __CONFIGWRAPPER_H__
#include <stdbool.h>
#include "InternalVoxelFormat.h"

struct configWrapper
{
   const struct config * config;
   size_t xDelta;
   size_t yDelta;
   size_t zDelta;
   size_t size;
};

static inline
void ConfigWrapper_Init(struct configWrapper * wrapper, 
                        const struct config * config)
{
   wrapper->config = config;
   wrapper->xDelta = 1;
   wrapper->yDelta = config->width;
   wrapper->zDelta = wrapper->yDelta * config->height;
   wrapper->size   = wrapper->zDelta * config->depth;
}

static inline
bool ConfigWrapper_IsIndexValid(const struct configWrapper * wrapper,
                                size_t index)
{
   return index < wrapper->size;
}

static inline
bool ConfigWrapper_IsPositionValid(const struct configWrapper * wrapper,
                                   int x, int y, int z)
{
   return x >= 0 && x < (int)wrapper->config->width  &&
          y >= 0 && y < (int)wrapper->config->height &&
          z >= 0 && z < (int)wrapper->config->depth;
}

static inline
size_t ConfigWrapper_GetBrushIndex(const struct configWrapper * wrapper,
                                   int x, int y, int z)
{
   if(!ConfigWrapper_IsPositionValid(wrapper, x, y, z))
   {
      return wrapper->size;
   }
   return x + y * wrapper->yDelta + z * wrapper->zDelta;
}

static inline
const struct brush * ConfigWrapper_GetBrushFromIndex(const struct configWrapper * wrapper,
                                                     size_t index)
{
   if(!ConfigWrapper_IsIndexValid(wrapper, index))
   {
      return NULL;
   }
   return wrapper->config->data[index];
}

static inline
const struct brush * ConfigWrapper_GetBrushFromPosition(const struct configWrapper * wrapper,
                                                        int x, int y, int z)
{
   size_t index = ConfigWrapper_GetBrushIndex(wrapper, x, y, z);
   if(!ConfigWrapper_IsIndexValid(wrapper, index))
   {
      return NULL;
   }
   return wrapper->config->data[index];
}

static inline
size_t ConfigWrapper_GetLargestDimention(const struct configWrapper * wrapper)
{
   size_t largest = wrapper->config->width;
   if(largest < wrapper->config->height)
   {
      largest = wrapper->config->height;
   }
   if(largest < wrapper->config->depth)
   {
      largest = wrapper->config->depth;
   }
   return largest;
}

#endif // __CONFIGWRAPPER_H__
