#ifndef __LIST_H__
#define __LIST_H__
#include <stddef.h>
#include <stdbool.h>

struct list
{
   size_t elementSize;
   size_t size;   // In Elements
   size_t count;  // In Elements
   size_t growBy; // In Elements
   unsigned char * data;
};

void List_Init(struct list * list, 
               size_t elementSize, 
               size_t growBy, 
               size_t initSize);

void List_Free(struct list * list);

void * List_AddEmpty(struct list * list, size_t * index);
void * List_AddCopy(struct list * list, const void * vp, size_t * index);
bool List_RemoveFast(struct list * list, size_t index);
bool List_Swap(struct list * list, size_t aIndex, size_t bIndex);

#define LIST_MAKE(postfix, type)                                               \
struct list_ ## postfix                                                        \
{                                                                              \
   struct list list;                                                           \
};                                                                             \
                                                                               \
static inline                                                                  \
void List_Init_ ## postfix (struct list_ ## postfix * list,                    \
                            size_t growBy, size_t initSize)                    \
{                                                                              \
   List_Init(&list->list, sizeof(type), growBy, initSize);                     \
}                                                                              \
                                                                               \
static inline                                                                  \
void List_Free_ ## postfix (struct list_ ## postfix * list)                    \
{                                                                              \
   List_Free(&list->list);                                                     \
}                                                                              \
                                                                               \
static inline                                                                  \
void List_Clear_ ## postfix (struct list_ ## postfix * list)                   \
{                                                                              \
   list->list.count = 0;                                                       \
}                                                                              \
                                                                               \
static inline                                                                  \
type * List_AddEmpty_ ## postfix (struct list_ ## postfix * list,              \
                                  size_t * index)                              \
{                                                                              \
   return List_AddEmpty(&list->list, index);                                   \
}                                                                              \
                                                                               \
static inline                                                                  \
type * List_Add_ ## postfix (struct list_ ## postfix * list, type v,           \
                             size_t * index)                                   \
{                                                                              \
   return List_AddCopy(&list->list, &v, index);                                \
}                                                                              \
                                                                               \
static inline                                                                  \
type * List_AddCopy_ ## postfix (struct list_ ## postfix * list, type * vp,    \
                                 size_t * index)                               \
{                                                                              \
   return List_AddCopy(&list->list, vp, index);                                \
}                                                                              \
                                                                               \
static inline                                                                  \
type * List_Get_ ## postfix (struct list_ ## postfix * list, size_t * count)   \
{                                                                              \
   if(count != NULL)                                                           \
   {                                                                           \
      (*count) = list->list.count;                                             \
   }                                                                           \
   return (type*)list->list.data;                                              \
}                                                                              \
                                                                               \
static inline                                                                  \
bool List_IsValidIndex_ ## postfix (struct list_ ## postfix * list,            \
                                    size_t index)                              \
{                                                                              \
   return index < list->list.count;                                            \
}                                                                              \
                                                                               \
static inline                                                                  \
size_t List_GetCount_ ## postfix (struct list_ ## postfix * list)              \
{                                                                              \
   return list->list.count;                                                    \
}                                                                              \
                                                                               \
static inline                                                                  \
bool List_RemoveFast_ ## postfix (struct list_ ## postfix * list,              \
                                 size_t index)                                 \
{                                                                              \
   return List_RemoveFast(&list->list, index);                                 \
}                                                                              \
                                                                               \
static inline                                                                  \
bool List_Swap_ ## postfix (struct list_ ## postfix * list,                    \
                            size_t aIndex, size_t bIndex)                      \
{                                                                              \
   return List_Swap(&list->list, aIndex, bIndex);                              \
}                                                                              \
                                                                               \

#endif // __LIST_H__

