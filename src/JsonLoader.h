#ifndef __JSONLOADER_H__
#define __JSONLOADER_H__
#include "InternalVoxelFormat.h"

struct jsonLoader;

struct jsonLoader * JsonLoader_Load(const char * filename);
struct jsonLoader * JsonLoader_LoadString(const char * string);
void JsonLoader_Destroy(struct jsonLoader * loader);


struct config * JsonLoader_GetConfig(struct jsonLoader * loader);



#endif // __JSONLOADER_H__

