#ifndef __MESHER_H__
#define __MESHER_H__
#include "InternalVoxelFormat.h"
#include "Mesh.h"

void Mesher_Mesh(struct mesh * mesh, const struct config * config);


#endif // __MESHER_H__
