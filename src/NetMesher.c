#include <stdio.h>
#include <stdbool.h>
#include <stdlib.h>
#include <math.h>
#include "NetMesher.h"
#include "ConfigWrapper.h"
#include "List.h"

#define NUM_CUBE_POINTS      8
#define NUM_CUBE_EDGES       12
#define NUM_CUBE_FACES       6
#define NUM_OCTTREE_CHILDREN 8
#define NUM_POINTS_PER_EDGE  2

/*
 * Axes are:
 *
 *      y
 *      |     z
 *      |   /
 *      | /
 *      +----- x
 *
 * Vertex and edge layout:
 *
 *            6             7
 *            +-------------+               +-----6-------+
 *          / |           / |             / |            /|
 *        /   |         /   |          11   7         10  5
 *    2 +-----+-------+  3  |         +-----+2------+     |
 *      |   4 +-------+-----+ 5       |     +-----4-+-----+
 *      |   /         |   /           3   8         1   9
 *      | /           | /             | /           | /
 *    0 +-------------+ 1             +------0------+
 */

static const unsigned char EdgeToPoints[NUM_CUBE_EDGES][NUM_POINTS_PER_EDGE] = {
   { 0, 1 },
   { 1, 3 },
   { 2, 3 },
   { 0, 2 },
   { 4, 5 },
   { 5, 7 },
   { 6, 7 },
   { 4, 6 },
   { 0, 4 },
   { 1, 5 },
   { 3, 7 },
   { 2, 6 },
};


struct vec3i
{
   int x;
   int y;
   int z;
};

static inline
struct vec3i * Vec3i_Copy(struct vec3i * dest, const struct vec3i * src)
{
   dest->x = src->x;
   dest->y = src->y;
   dest->z = src->z;
   return dest;
}

static inline
bool Vec3i_IsEqual(const struct vec3i * a, const struct vec3i * b)
{
   return a->x == b->x &&
          a->y == b->y &&
          a->z == b->z;
}

static inline
struct vec3i * Vec3i_Subtract(struct vec3i * out, const struct vec3i * a, 
                                                  const struct vec3i * b)
{
   out->x = a->x - b->x;
   out->y = a->y - b->y;
   out->z = a->z - b->z;
   return out;
}

static inline
struct vec3i * Vec3i_Add(struct vec3i * out, const struct vec3i * a, 
                                             const struct vec3i * b)
{
   out->x = a->x + b->x;
   out->y = a->y + b->y;
   out->z = a->z + b->z;
   return out;
}

static inline
struct vec3i * Vec3i_Between(struct vec3i * out, const struct vec3i * a, 
                                                 const struct vec3i * b)
{
   struct vec3i diff;
   (void)Vec3i_Subtract(&diff, b, a);
   diff.x /= 2;
   diff.y /= 2;
   diff.z /= 2;
   return Vec3i_Add(out, a, &diff);
}

static inline
unsigned int Vec3i_ManhattanLength(const struct vec3i * v)
{
   int x = v->x, y = v->y, z = v->z;
   if(x < 0)
   {
      x = -x;
   }
   if(y < 0)
   {
      y = -y;
   }
   if(z < 0)
   {
      z = -z;
   }
   return x + y + z;
}


enum nodeType
{
   eNT_Unknown,
   eNT_Branch,
   eNT_Empty,
   eNT_Full,
   eNT_Leaf,
};

static const char * NodeTypeString[5] = {
   "Unknown",
   "Branch", 
   "Empty",
   "Full",
   "Leaf",
};

enum edgeState
{
   eES_Unknown,
   eES_Inside,
   eES_Outside,
   eES_Transition,
};

struct node
{
   enum nodeType type;
   bool hasChildren;
   struct node * children[NUM_OCTTREE_CHILDREN];
   struct vec3i vecs[NUM_CUBE_POINTS];
   const struct brush * brushes[NUM_CUBE_POINTS];
   enum edgeState edgeStates[NUM_CUBE_EDGES];
   int depth;
   struct vec3f points[2];
   struct node * peers[NUM_CUBE_FACES];
   unsigned char peerCount;
};

LIST_MAKE(nodep, struct node * )

static inline
struct node * Node_Create(const struct vec3i vecs[NUM_CUBE_POINTS], int depth)
{
   size_t i;
   struct node * node = malloc(sizeof(struct node));
   node->type = eNT_Unknown;
   node->hasChildren = false;
   node->depth = depth;
   node->peerCount = 0;
   for(i = 0; i < NUM_CUBE_POINTS; i++)
   {
      (void)Vec3i_Copy(&node->vecs[i], &vecs[i]);
   }
   for(i = 0; i < NUM_OCTTREE_CHILDREN; i++)
   {
      node->children[i] = NULL;
   }
   for(i = 0; i < NUM_CUBE_FACES; i++)
   {
      node->peers[i] = NULL;
   }
   
   // Center the point
   node->points[0].x = (vecs[0].x + vecs[7].x) / 2.0f;
   node->points[0].y = (vecs[0].y + vecs[7].y) / 2.0f;
   node->points[0].z = (vecs[0].z + vecs[7].z) / 2.0f;

   return node;
}

static inline
void Node_Destroy(struct node * node)
{
   if(node->hasChildren)
   {
      size_t i;
      for(i = 0; i < NUM_OCTTREE_CHILDREN; i++)
      {
         Node_Destroy(node->children[i]);
      }
   }
   free(node);
}

static inline
struct node * Node_CreateRoot(const struct configWrapper * cWrapper)
{
   const struct netSetting * ns = &cWrapper->config->settings.net;
   // Find Largest Dimension
   double largest = (ConfigWrapper_GetLargestDimention(cWrapper) + 
                     (ns->smallestOctCube * 2)) /
                     (double)ns->smallestOctCube;

   // Compute Number Of divisions
   int divisions = (int)ceil(log(largest) / log(2));

   // Compute Final Size
   size_t size = ((size_t)pow(2, divisions)) * ns->smallestOctCube;
   int min = -ns->smallestOctCube;
   int max = size - ns->smallestOctCube;

   const struct vec3i vecs[8] = {
      { min, min, min },
      { max, min, min },
      { min, max, min },
      { max, max, min },
      { min, min, max },
      { max, min, max },
      { min, max, max },
      { max, max, max },
   };
   printf("smallestOctCube: %d, divisions: %d, size: %d\n",
          (int)ns->smallestOctCube, divisions, (int)size);
   struct node * node = Node_Create(vecs, divisions);
   return node;
}

static const unsigned char ParentToPointCube[NUM_CUBE_POINTS] = {
   0, 2, 6, 8, 18, 20, 24, 26
};

static const unsigned char SplitsOrder[57] = {
   // Do Edges
   0,  2,  1,
   2,  8,  5,
   6,  8,  7,
   0,  6,  3,
   18, 20, 19,
   20, 26, 23,
   24, 26, 25,
   18, 24, 21,
   0,  18, 9,
   2,  20, 11,
   6,  24, 15,
   8,  26, 17,
   // Side Centers
   3,  5,  4,
   21, 23, 22,
   3,  21, 12,
   5,  23, 14,
   1,  19, 10,
   7,  25, 16,
   // Cube Center
   4,  22, 13,
};

static const unsigned char ChildIndexes[NUM_OCTTREE_CHILDREN][NUM_CUBE_POINTS] = {
   { 0,  1,  3,  4,  9,  10, 12, 13 },
   { 1,  2,  4,  5,  10, 11, 13, 14 },
   { 3,  4,  6,  7,  12, 13, 15, 16 },
   { 4,  5,  7,  8,  13, 14, 16, 17 },
   { 9,  10, 12, 13, 18, 19, 21, 22 },
   { 10, 11, 13, 14, 19, 20, 22, 23 },
   { 12, 13, 15, 16, 21, 22, 24, 25 },
   { 13, 14, 16, 17, 22, 23, 25, 26 },
}; 


static inline
void Node_MakeChildren(struct node * node)
{
   if(!node->hasChildren)
   {
      struct vec3i pointCube[27];
      unsigned char i;
      for(i = 0; i < NUM_CUBE_POINTS; i++)
      {
         (void)Vec3i_Copy(&pointCube[ParentToPointCube[i]],  &node->vecs[i]);
      }
      for(i = 0; i < sizeof(SplitsOrder); i += 3)
      {
         const struct vec3i * a = &pointCube[SplitsOrder[i]];
         const struct vec3i * b = &pointCube[SplitsOrder[i + 1]];
         struct vec3i * out     = &pointCube[SplitsOrder[i + 2]];
         (void)Vec3i_Between(out, a, b);
      }
      for(i = 0; i < NUM_OCTTREE_CHILDREN; i ++)
      {
         struct vec3i childPoints[NUM_CUBE_POINTS];

         unsigned char k;
         for(k = 0; k < NUM_CUBE_POINTS; k++)
         {
            unsigned char pci = ChildIndexes[i][k];
            (void)Vec3i_Copy(&childPoints[k], &pointCube[pci]);
         } 
         node->children[i] = Node_Create(childPoints, node->depth - 1);
      }
      node->hasChildren = true;
      node->type = eNT_Branch;
   }
   
}

static inline
bool Brush_IsEmpty(const struct brush * brush)
{
   return brush == NULL || brush->isEmpty;
}

static inline
void Node_ComputeType(struct node * node, const struct configWrapper * cWrapper)
{
   unsigned char i;
   bool allEmpty = true;
   bool allFull = true;
   // Store brushes
   for(i = 0; i < NUM_CUBE_POINTS; i++)
   {
      const struct vec3i * p = &node->vecs[i];
      node->brushes[i] = ConfigWrapper_GetBrushFromPosition(cWrapper, 
                                                            p->x, p->y, p->z);
      if(Brush_IsEmpty(node->brushes[i]))
      {
         allFull = false;
      }
      else
      {
         allEmpty = false;
      }
   }
   for(i = 0; i < NUM_CUBE_EDGES; i++)
   {
      bool isEmpty[2] = {
         Brush_IsEmpty(node->brushes[EdgeToPoints[i][0]]),
         Brush_IsEmpty(node->brushes[EdgeToPoints[i][1]]),
      };
      if(isEmpty[0] == isEmpty[1])
      {
         if(isEmpty[0])
         {
            node->edgeStates[i] = eES_Outside;
         }
         else
         {
            node->edgeStates[i] = eES_Inside;
         }
      }
      else
      {
         node->edgeStates[i] = eES_Transition;
      }
   }
   if(allEmpty)
   {
      node->type = eNT_Empty;
   }
   else if(allFull)
   {
      node->type = eNT_Full;
   }
   else
   {
      node->type = eNT_Leaf;
   }
}

static inline
void Node_Collapse(struct node * node)
{
   unsigned char i;
   for(i = 0; i < NUM_OCTTREE_CHILDREN; i++)
   {
      Node_Destroy(node->children[i]);
      node->children[i] = NULL;
   }
   node->hasChildren = false;
}

static inline
void Node_CollapseIfPossible(struct node * node)
{
   if(node->hasChildren)
   {
      bool childrenDontHaveChildren = true;
      bool allEmpty = true;
      bool allFull = true;
      unsigned char i;
      for(i = 0; i < NUM_OCTTREE_CHILDREN; i++)
      {
         struct node * child = node->children[i];
         if(child->hasChildren)
         {
            childrenDontHaveChildren = false;
         }
         if(child->type != eNT_Full)
         {
            allFull = false;
         }
         if(child->type != eNT_Empty)
         {
            allEmpty = false;
         }
      }
      // TODO: Check to see if smooth enough to collapse if we have leafs

      if(childrenDontHaveChildren && (allFull || allEmpty))
      {
         Node_Collapse(node);
         if(allFull)
         {
            node->type = eNT_Full;
         }
         else if(allEmpty)
         {
            node->type = eNT_Empty;
         }
         else
         {
            node->type = eNT_Leaf;
         }
      }
   }   
}

static inline
void Node_BuildTree(struct node * node, const struct configWrapper * cWrapper)
{
   Node_ComputeType(node, cWrapper);
   if(node->depth > 0)
   {
      unsigned char i;
      Node_MakeChildren(node);
      for(i = 0; i < NUM_OCTTREE_CHILDREN; i++)
      {
         Node_BuildTree(node->children[i], cWrapper);
      }
      Node_CollapseIfPossible(node);
   }
}



#define NUM_FACEPROC_NODES 2
#define NUM_FACEPROC_PER_FACEPROC 4
#define NUM_EDGEPROC_PER_FACEPROC 4

#define NUM_EDGEPROC_NODES 4
#define NUM_EDGEPROC_PER_EDGEPROC 2

enum octDir
{
   eOD_LeftRight,
   eOD_BottomTop,
   eOD_FrontBack,
   eOD_Size,
};

enum proc
{
   eP_Cell,
   eP_Face,
   eP_Edge,
   eP_Size,
};

struct nodeListStats
{
   struct node ** nodes;
   unsigned char count;
   bool oneNotALeaf;
   bool allHaveAFace;
};

static inline
struct nodeListStats * NodeListStats_Compute(struct nodeListStats * stats, 
                                             struct node ** nodes, 
                                             unsigned char count)
{
   unsigned char i;
   stats->oneNotALeaf = false;
   stats->allHaveAFace = true;
   stats->nodes = nodes;
   stats->count = count;
   for(i = 0; i < count; i++)
   {
      const struct node * n = nodes[i];
      if(n->type != eNT_Leaf)
      {
         stats->oneNotALeaf = true;
      }
      if(n->type == eNT_Empty ||
         n->type == eNT_Full)
      {
         stats->allHaveAFace = false;
      }
   }
   return stats;
}

static const unsigned char UnfoldFaceLookup[eOD_Size][NUM_POINTS_PER_EDGE][NUM_EDGEPROC_NODES] = {
   /* eOD_LeftRight = */ {
      { 0, 1, 3, 2 }, { 0, 2, 3, 1 },
   },
   /* eOD_BottomTop = */ {
      { 0, 2, 3, 1 }, { 0, 1, 3, 2 },
   },
   /* eOD_FrontBack = */ {
      { 0, 1, 3, 2 }, { 0, 2, 3, 1 },
   },
};


static const unsigned char EdgeFromDirAndNodeIndex[eOD_Size][NUM_EDGEPROC_NODES] = {
   /* eOD_LeftRight = */ {
      6, 4, 2, 0,
   },
   /* eOD_BottomTop = */ {
      5, 7, 1, 3
   },
   /* eOD_FrontBack = */ {
      10, 11, 9, 8
   },
   
};

static const unsigned char EdgeProcLookupForEdgeProc[eOD_Size][NUM_EDGEPROC_PER_EDGEPROC][NUM_EDGEPROC_NODES] = {
   /* eOD_LeftRight = */ {
      { 6, 4, 2, 0 }, { 7, 5, 3, 1 },
   },
   /* eOD_BottomTop = */ {
      { 5, 4, 1, 0 }, { 7, 6, 3, 2 },
   },
   /* eOD_FrontBack = */ {
      { 3, 2, 1, 0 }, { 7, 6, 5, 4 },
   },
};

static inline
void Node_AddPeerOneWay(struct node * node, struct node * peer)
{
   bool linkExists = false;
   unsigned char i;
   for(i = 0; i < node->peerCount; i++)
   {
      if(node->peers[i] == peer)
      {
         linkExists = true;
         break;
      }
   }
   if(!linkExists)
   {
      node->peers[node->peerCount] = peer;
      node->peerCount ++;
   }
}

static inline
void Node_JoinPeers(struct node * a, struct node * b)
{
   Node_AddPeerOneWay(a, b);
   Node_AddPeerOneWay(b, a);
}

static inline
void Node_BuildMesh_AddFace(struct mesh * mesh, 
                            struct node * nodes[NUM_EDGEPROC_NODES],
                            enum octDir dir,
                            unsigned char node0EdgeIndex)
{
   const struct brush * node0EdgeBrush = nodes[0]->brushes[EdgeToPoints[node0EdgeIndex][0]];
   unsigned char facingIndex = Brush_IsEmpty(node0EdgeBrush) ? 1 : 0;
   const struct vec3f * points[NUM_EDGEPROC_NODES];
   
   const unsigned char * unfoldLookup = UnfoldFaceLookup[dir][facingIndex];
   struct node * culledNodes[NUM_EDGEPROC_NODES];
   // Unwind the face and remove duplicates
   unsigned char i;
   unsigned char nodeCount = 0;
   struct node * prevNode = NULL;
   for(i = 0; i < NUM_EDGEPROC_NODES; i++)
   {
      struct node * node = nodes[unfoldLookup[i]];
      if(node != prevNode)
      {
         culledNodes[nodeCount] = node;
         points[nodeCount] = &node->points[0];
         nodeCount++;
         prevNode = node;
      }
   }
   Mesh_AddFace(mesh, points, nodeCount);

   unsigned char prevIndex = nodeCount - 1;
   // Link Nodes with each other
   for(i = 0; i < nodeCount; i++)
   {
      Node_JoinPeers(culledNodes[prevIndex], culledNodes[i]);
      prevIndex = i; 
   }   
}

static 
void Node_BuildMesh_EdgeProc(struct mesh * mesh, 
                             struct node * nodes[NUM_EDGEPROC_NODES], 
                             enum octDir dir)
{
   struct nodeListStats stats;
   (void)NodeListStats_Compute(&stats, nodes, NUM_FACEPROC_NODES);
   if(!stats.oneNotALeaf)
   {
      // All Leafs, so we have a face.
      // But is it a transition Edge?
      unsigned char node0EdgeIndex = EdgeFromDirAndNodeIndex[dir][0];
      enum edgeState node0EdgeState = nodes[0]->edgeStates[node0EdgeIndex];
      if(node0EdgeState == eES_Transition)
      {
         // It is a transition edge! Lets add the face
         Node_BuildMesh_AddFace(mesh, nodes, dir, node0EdgeIndex);
      }
      
   }
   else if(stats.allHaveAFace)
   {
      unsigned char i;
      for(i = 0; i < NUM_EDGEPROC_PER_EDGEPROC; i ++)
      {
         const unsigned char * lookup = EdgeProcLookupForEdgeProc[dir][i];
         struct node * nextNodes[NUM_EDGEPROC_NODES];
         unsigned char k;
         for(k = 0; k < NUM_EDGEPROC_NODES; k++)
         {
            if(nodes[k]->hasChildren)
            {
               nextNodes[k] = nodes[k]->children[lookup[k]];
            }
            else
            {
               nextNodes[k] = nodes[k];
            }
         }
         Node_BuildMesh_EdgeProc(mesh, nextNodes, dir);
      }
   }
}

static const unsigned char FaceProcLookupForFaceProc[eOD_Size][NUM_FACEPROC_PER_FACEPROC][NUM_FACEPROC_NODES] = {
   /* eOD_LeftRight = */ {
      { 1, 0 }, { 3, 2, }, { 5, 4 }, { 7, 6 },
   },
   /* eOD_BottomTop = */ {
      { 2, 0 }, { 3, 1, }, { 6, 4 }, { 7, 5 },
   },
   /* eOD_FrontBack = */ {
      { 4, 0 }, { 5, 1, }, { 6, 2 }, { 7, 3 },
   },
};

enum edgeOrder
{
   eEO_aabb,
   eEO_abab,
   eEO_Size,
};

static const unsigned char ProcEdgeOrder[eEO_Size][4] = {
   { 0, 0, 1, 1 },
   { 0, 1, 0, 1 },
};

static const unsigned char FaceProcLookupForEdgeProc[eOD_Size][NUM_EDGEPROC_PER_FACEPROC][NUM_EDGEPROC_NODES + 2] = {
   /* eOD_LeftRight = */ {
      { 1, 0, 3, 2, eEO_abab, eOD_FrontBack },
      { 5, 4, 7, 6, eEO_abab, eOD_FrontBack },
      { 1, 0, 5, 4, eEO_abab, eOD_BottomTop },
      { 3, 2, 7, 6, eEO_abab, eOD_BottomTop },
   },
   /* eOD_BottomTop = */ {
      { 2, 0, 6, 4, eEO_abab, eOD_LeftRight },
      { 3, 1, 7, 5, eEO_abab, eOD_LeftRight },
      { 2, 3, 0, 1, eEO_aabb, eOD_FrontBack },
      { 6, 7, 4, 5, eEO_aabb, eOD_FrontBack },
   },
   /* eOD_FrontBack = */ {
      { 4, 6, 0, 2, eEO_aabb, eOD_LeftRight },
      { 5, 7, 1, 3, eEO_aabb, eOD_LeftRight },
      { 4, 5, 0, 1, eEO_aabb, eOD_BottomTop },
      { 6, 7, 2, 3, eEO_aabb, eOD_BottomTop },
   },
};


static 
void Node_BuildMesh_FaceProc(struct mesh * mesh,
                             struct node * nodes[NUM_FACEPROC_NODES], 
                             enum octDir dir)
{
   struct nodeListStats stats;
   (void)NodeListStats_Compute(&stats, nodes, NUM_FACEPROC_NODES);
   if(stats.oneNotALeaf && stats.allHaveAFace)
   {
      unsigned char i;
      // FaceProcs
      for(i = 0; i < NUM_FACEPROC_PER_FACEPROC; i++)
      {
         const unsigned char * lookup = FaceProcLookupForFaceProc[dir][i];
         struct node * nextNodes[NUM_FACEPROC_NODES];
         unsigned char k;
         for(k = 0; k < NUM_FACEPROC_NODES; k++)
         {
            if(nodes[k]->hasChildren) 
            {
               nextNodes[k] = nodes[k]->children[lookup[k]];
            }
            else
            {
               nextNodes[k] = nodes[k];
            }
         }
         Node_BuildMesh_FaceProc(mesh, nextNodes, dir);
      }
      // EdgeProcs
      for(i = 0; i < NUM_EDGEPROC_PER_FACEPROC; i++)
      {
         const unsigned char * lookup = FaceProcLookupForEdgeProc[dir][i];
         const unsigned char * eo = ProcEdgeOrder[lookup[4]];
         struct node * nextNodes[NUM_EDGEPROC_NODES];
         enum octDir nextDir = lookup[5];
         unsigned char k;
         for(k = 0; k < NUM_EDGEPROC_NODES; k++)
         {
            struct node * srcNode = nodes[eo[k]];
            if(srcNode->hasChildren)
            {
               nextNodes[k] = srcNode->children[lookup[k]];
            }
            else
            {
               nextNodes[k] = srcNode;
            }
         }
         Node_BuildMesh_EdgeProc(mesh, nextNodes, nextDir);
      }
   }
}

static const unsigned char CellProcProgramJump[eP_Size] ={
   /* eP_Cell = */ 2, 
   /* eP_Face = */ 2 + NUM_FACEPROC_NODES,
   /* eP_Edge = */ 2 + NUM_EDGEPROC_NODES,
};
static const unsigned char CellProcProgram[] = {
   // Cells
   eP_Cell, 0,
   eP_Cell, 1,
   eP_Cell, 2,
   eP_Cell, 3, 
   eP_Cell, 4,
   eP_Cell, 5,
   eP_Cell, 6,
   eP_Cell, 7,
   // Faces
   eP_Face, 0, 1, eOD_LeftRight,
   eP_Face, 2, 3, eOD_LeftRight,
   eP_Face, 4, 5, eOD_LeftRight,
   eP_Face, 6, 7, eOD_LeftRight,
   eP_Face, 0, 2, eOD_BottomTop,
   eP_Face, 1, 3, eOD_BottomTop,
   eP_Face, 4, 6, eOD_BottomTop,
   eP_Face, 5, 7, eOD_BottomTop,
   eP_Face, 0, 4, eOD_FrontBack,
   eP_Face, 1, 5, eOD_FrontBack,
   eP_Face, 2, 6, eOD_FrontBack,
   eP_Face, 3, 7, eOD_FrontBack,
   // Edges
   eP_Edge, 0, 1, 2, 3, eOD_FrontBack,
   eP_Edge, 4, 5, 6, 7, eOD_FrontBack,
   eP_Edge, 0, 1, 4, 5, eOD_BottomTop,
   eP_Edge, 2, 3, 6, 7, eOD_BottomTop,
   eP_Edge, 0, 2, 4, 6, eOD_LeftRight,
   eP_Edge, 1, 3, 5, 7, eOD_LeftRight,
};

static 
void Node_BuildMesh_CellProc(struct mesh * mesh, struct node * node)
{
   if(node->hasChildren)
   {
      unsigned char i = 0;
      while(i < sizeof(CellProcProgram))
      {
         const unsigned char * step = &CellProcProgram[i];
         enum proc proc = step[0];
         enum octDir dir;
         struct node * nodes[4];
         switch(proc)
         {
         case eP_Cell:
            Node_BuildMesh_CellProc(mesh, node->children[step[1]]);
            break;
         case eP_Face:
            nodes[0] = node->children[step[1]];
            nodes[1] = node->children[step[2]];
            dir      =                step[3];
            Node_BuildMesh_FaceProc(mesh, nodes, dir);
            break;
         case eP_Edge:
            nodes[0] = node->children[step[1]];
            nodes[1] = node->children[step[2]];
            nodes[2] = node->children[step[3]];
            nodes[3] = node->children[step[4]];
            dir      =                step[5];
            Node_BuildMesh_EdgeProc(mesh, nodes, dir);
            break;
         default:
            // Do nothing intentionally 
            break;
         }
         i += CellProcProgramJump[proc];
      }
   }
}

static inline
void Node_BuildMesh(struct node * node, struct mesh * mesh)
{
   Node_BuildMesh_CellProc(mesh, node);
}

static const unsigned char EdgeIndexToCoordIndex[NUM_CUBE_EDGES] = {
   0, 1, 0, 1, 0, 1, 0, 1, 2, 2, 2, 2 
};

static inline
float Node_FindTranstionAlongEdge(const struct node * node, 
                                  unsigned char edgeIndex,
                                  const struct configWrapper * cWrapper)
{
   float result = -500;
   const unsigned char coordIndex = EdgeIndexToCoordIndex[edgeIndex];
   const unsigned char * pointIndexes = EdgeToPoints[edgeIndex];
   const struct vec3i * from = &node->vecs[pointIndexes[0]];
   const struct vec3i * to   = &node->vecs[pointIndexes[1]];
   struct vec3i diff, norm, point;
   (void)Vec3i_Subtract(&diff, to, from);
   (void)Vec3i_Copy(&point, from);
   unsigned int len = Vec3i_ManhattanLength(&diff);
   norm.x = diff.x / len;
   norm.y = diff.y / len;
   norm.z = diff.z / len;
   
   bool prevEmpty = Brush_IsEmpty(node->brushes[pointIndexes[0]]);
   (void)Vec3i_Add(&point, &point, &norm);
   
   unsigned int i;
   for(i = 0; i < len; i++)
   {
      const struct brush * brush = ConfigWrapper_GetBrushFromPosition(cWrapper, 
                                                                      point.x,
                                                                      point.y,
                                                                      point.z);
      bool isEmpty = Brush_IsEmpty(brush);
      if(isEmpty != prevEmpty)
      {
         float fp[3] = {point.x, point.y, point.z};
         float fn[3] = {norm.x, norm.y, norm.z};
         result = fp[coordIndex] - (fn[coordIndex] * 0.5f);
         break;
      }
      
      (void)Vec3i_Add(&point, &point, &norm);
   }
   
   
   return result;
}

static 
void Node_AddLeafsToList(struct node * node, struct list_nodep * list)
{
   if(node->type == eNT_Leaf)
   {
      (void)List_Add_nodep(list, node, NULL);
   }
   else if(node->hasChildren)
   {
      unsigned char i;
      for(i = 0; i < NUM_OCTTREE_CHILDREN; i++)
      {
         Node_AddLeafsToList(node->children[i], list);
      }
   }
}

static 
float NodeList_ComputeEnergy(struct list_nodep * list, 
                             unsigned char pointsIndex)
{
   float doubleEnergy = 0;
   size_t i, count;
   struct node ** nodes = List_Get_nodep(list, &count);
   for(i = 0; i < count; i++)
   {
      struct node * a = nodes[i];
      unsigned char k;
      for(k = 0; k < a->peerCount; k++)
      {
         struct node * b = a->peers[k];
         struct vec3f diff;
         (void)Vec3f_Subtract(&diff, &b->points[pointsIndex],
                                     &a->points[pointsIndex]);
         doubleEnergy += Vec3f_Length2(&diff);
         
      }
   }
   return doubleEnergy / 2.0f;
}

static inline
void NodeList_SmoothIteration(struct list_nodep * list, unsigned char from,
                                                        unsigned char to)
{
   size_t i, count;
   struct node ** nodes = List_Get_nodep(list, &count);
   for(i = 0; i < count; i++)
   {
      struct node * node = nodes[i];
      struct vec3f * v = &node->points[to];
      const struct vec3i * min = &node->vecs[0];
      const struct vec3i * max = &node->vecs[7];
      Vec3f_Zero(v);
      unsigned char k;
      for(k = 0; k < node->peerCount; k++)
      {
         Vec3f_Add(v, v, &node->peers[k]->points[from]);
      }
      v->x /= node->peerCount; 
      v->y /= node->peerCount; 
      v->z /= node->peerCount;



      // Force new point inside OctNode
      if(v->x < min->x)
      {
         v->x = min->x;
      } 
      else if(v->y > max->x)
      {
         v->x = max->x;
      }

      if(v->y < min->y)
      {
         v->y = min->y;
      }
      else if(v->y > max->y)
      {
         v->y = max->y;
      }

      if(v->z < min->z)
      {
         v->z = min->z;
      }
      else if(v->z > max->z)
      {
         v->z = max->z;
      }
   }
}

static inline
void NodeList_SwapPoints(struct list_nodep * list)
{
   
   size_t i, count;
   struct node ** nodes = List_Get_nodep(list, &count);
   for(i = 0; i < count; i++)
   {
      struct node * node = nodes[i];
      struct vec3f temp;
      (void)Vec3f_Copy(&temp, &node->points[0]);
      (void)Vec3f_Copy(&node->points[0], &node->points[1]);
      (void)Vec3f_Copy(&node->points[1], &temp);
   }
}

static inline
void Node_Smooth(struct node * node, size_t maxSmoothingSteps)
{
   unsigned char from = 1, to = 0;
   size_t step = 0;
   bool done = false;
   struct list_nodep list;
   List_Init_nodep(&list, 0, 0);
   Node_AddLeafsToList(node, &list);
   
   float prevEnergy = NodeList_ComputeEnergy(&list, 0);
   while(!done)
   {
      // Swap Points
      float energy = 0;
      unsigned char temp = from;
      from = to;
      to = temp;

      // Smooth and Compute new energy
      NodeList_SmoothIteration(&list, from, to);
      energy = NodeList_ComputeEnergy(&list, to);
      step ++;
     
      float energyDiff = prevEnergy - energy; 

      printf("Step %d: EnergyDiff: %f\n", (int)step, energyDiff);
      // Figure out if we are done
      if(energyDiff < 0.1 || step >= maxSmoothingSteps)
      {
         done = true;
      }

      prevEnergy = energy;
      
   }

   if(to != 0)
   {
      NodeList_SwapPoints(&list); 
   }

   List_Free_nodep(&list);
}

static 
void Node_Fit(struct node * node, const struct configWrapper * cWrapper)
{
   if(node->hasChildren)
   {
      unsigned char i;
      for(i = 0; i < NUM_OCTTREE_CHILDREN; i++)
      {
         Node_Fit(node->children[i], cWrapper);
      }
   }
   else if(node->type == eNT_Leaf)
   {
      unsigned char i;
      float p[3] = { node->points[0].x, node->points[0].y, node->points[0].z };
      for(i = 0; i < NUM_CUBE_EDGES; i++)
      {
         if(node->edgeStates[i] == eES_Transition)
         {
            const unsigned char coordIndex = EdgeIndexToCoordIndex[i];
            float transition = Node_FindTranstionAlongEdge(node, i, cWrapper);
            p[coordIndex] = transition;
         }
      }
      node->points[0].x = p[0];
      node->points[0].y = p[1];
      node->points[0].z = p[2];
   }
}


// This function is more for debugging purposes
static inline
void Node_PrintTree(const struct node * node, int indentLevel)
{
   int indentSpacing = indentLevel * 2;
   unsigned char i;
   printf("%*s%d (%d, %d, %d) (%d, %d, %d) %s\n", indentSpacing, "", node->depth,
          (int)node->vecs[0].x, (int)node->vecs[0].y, (int)node->vecs[0].z,
          (int)node->vecs[7].x, (int)node->vecs[7].y, (int)node->vecs[7].z,
          NodeTypeString[node->type]);
   if(node->hasChildren)
   {
      for(i = 0; i < NUM_OCTTREE_CHILDREN; i++)
      {
         Node_PrintTree(node->children[i], indentLevel + 1);
      }
   }
}




void NetMesher_Mesh(struct mesh * mesh, const struct config * config)
{
   struct configWrapper configWrapper;
   struct configWrapper * cWrapper = &configWrapper;
   const struct netSetting * sNet  = &config->settings.net;
   ConfigWrapper_Init(cWrapper, config);
   struct node * root = Node_CreateRoot(cWrapper);
   Node_BuildTree(root, cWrapper);
   printf("\nPrinting Tree:\n");
   Node_PrintTree(root, 1);
   Node_BuildMesh(root, mesh);
   Node_Fit(root, cWrapper);
   if(sNet->maxSmoothingSteps > 0)
   {
      Node_Smooth(root, sNet->maxSmoothingSteps);
   }
   Mesh_Solidify(mesh);
   Node_Destroy(root);
}


