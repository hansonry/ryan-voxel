#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "InternalVoxelFormat.h"
#include "Mesh.h"
#include "Mesher.h"
#include "JsonLoader.h"


static inline
const char * GetProgramName(const char * arg0)
{
   const char * r1 = strrchr(arg0, '/');
   const char * r2 = strrchr(arg0, '/');
   if(r1 == NULL && r2 == NULL)
   {
      return arg0;
   }
   else if(r1 == NULL)
   {
      return r2 + 1;
   }
   else if(r2 == NULL)
   {
      return r1 + 1;
   }
   if(r2 > r1)
   {
      return r2 + 1;
   }
   else
   {
      return r1 + 1;
   }
}

static inline
void PrintHelp(const char * arg0)
{
   const char * name = GetProgramName(arg0); 
   printf("\n");
   printf("%s - Making a game ready mesh from voxel data.\n", name);
   printf("\n"); 
   printf("Usage:\n"); 
   printf("%s <json file>\n", name);
   printf("\n");

}

int main(int argc, char * args[])
{
   const struct config * config = NULL;
   struct jsonLoader * loader = NULL;
   

   if(argc > 1)
   {
      const char * filename = args[1];
      loader = JsonLoader_Load(filename);
      if(loader == NULL)
      {
         fprintf(stderr, "Failed to load file %s - Printing Help.\n", filename);
         PrintHelp(args[0]);
         return -1;
      }
      else
      {
         config = JsonLoader_GetConfig(loader);
      }     
   }
   else
   {
      fprintf(stderr, "No input file specified - Printing Help.\n");
      PrintHelp(args[0]);
      return -1;
   }
   struct mesh * mesh = Mesh_Create();

   Mesher_Mesh(mesh, config);

   Mesh_MakeObjFile(mesh, config->name);

   
   if(loader != NULL)
   {
      JsonLoader_Destroy(loader);
      loader = NULL;
   }
 
   Mesh_Destroy(mesh);
   printf("End Of Program\n");
   return 0;
}

