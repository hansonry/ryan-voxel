# Ryan Voxel

A tool to turn voxel data into a game ready mesh and image maps


## Building

Should be able to use cmake after change directory to the project root.

```bash
cmake .
make
```

On ubuntu systems you may need to specify the path of the `cJson` library by
adding `-DcJSON_DIR=/usr/lib/x86_64-linux-gnu/cmake/cJSON` to the `cmake`
command. You may need to change the path depending on your system. Not sure
why this is nessary.

### Dependencies

This tool needs the following libraries:

* cJson
* png 

To build you need the following:
 * c compiler
 * make
 * cmake

On Ubuntu based systems you can use the following apt-get line

```bash
apt-get install libcjson-dev libpng-dev build-essential cmake

```

